﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

public class Quadtree<T>
{
    private const int MaxItemsPerNode = 10;

    private class Node
    {
        public Node[] Children { get; } = new Node[4];
        public List<(double, double, T)> Items { get; } = new List<(double, double, T)>();
    }

    private Node root;

    public Quadtree(double minX, double minY, double maxX, double maxY)
    {
        root = new Node();
    }

    public void Insert(double x, double y, T value)
    {
        Insert(root, 0, 0, 0, double.MaxValue, double.MaxValue, x, y, value);
    }

    private void Insert(Node node, int depth, double minX, double minY, double maxX, double maxY, double x, double y, T value)
    {
        if (node != null)
        {
            if (node.Children[0] == null)
            {
                node.Items.Add((x, y, value));

                if (node.Items.Count > MaxItemsPerNode && depth < 10)
                {
                    Subdivide(node, depth);
                }
            }
            else
            {
                double midX = (minX + maxX) / 2;
                double midY = (minY + maxY) / 2;

                int quadrant = 0;
                if (x >= midX) quadrant |= 1;
                if (y >= midY) quadrant |= 2;

                Insert(node.Children[quadrant], depth + 1,
                       (quadrant & 1) == 0 ? minX : midX, (quadrant & 2) == 0 ? minY : midY,
                       (quadrant & 1) == 0 ? midX : maxX, (quadrant & 2) == 0 ? midY : maxY, x, y, value);
            }
        }
    }


    public List<T> FindNearest(double x, double y)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        List<T> result = new List<T>();
        FindNearest(root, x, y, result);

        stopwatch.Stop();
        Console.WriteLine($"Lookup time: {stopwatch.Elapsed.TotalMilliseconds} ms");

        return result;
    }

    private void FindNearest(Node node, double x, double y, List<T> result)
    {
        if (node != null)
        {
            double closestDistance = double.MaxValue;
            List<T> closestItems = new List<T>();

            foreach (var item in node.Items)
            {
                double distance = CalculateDistance(x, y, item.Item1, item.Item2);

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestItems.Clear();
                    closestItems.Add(item.Item3);
                }
                else if (distance == closestDistance)
                {
                    closestItems.Add(item.Item3);
                }
            }

            result.AddRange(closestItems);

            double[] distances = new double[4];
            double closestQuadrantDistance = double.MaxValue;
            int closestQuadrant = -1;

            for (int i = 0; i < 4; i++)
            {
                distances[i] = CalculateDistanceToQuadrant(x, y, node, i);
                if (distances[i] < closestQuadrantDistance)
                {
                    closestQuadrantDistance = distances[i];
                    closestQuadrant = i;
                }
            }

            if (closestQuadrant >= 0)
            {
                FindNearest(node.Children[closestQuadrant], x, y, result);
                for (int i = 0; i < 4; i++)
                {
                    if (i != closestQuadrant && distances[i] < closestQuadrantDistance)
                    {
                        FindNearest(node.Children[i], x, y, result);
                    }
                }
            }
        }
    }

    private double CalculateDistanceToQuadrant(double x, double y, Node node, int quadrant)
    {
        if (node.Children[quadrant] != null && node.Children[quadrant].Items.Count > 0)
        {
            double minX, minY, maxX, maxY;

            if ((quadrant & 1) == 0)
            {
                minX = node.Children[quadrant].Items.Min(i => i.Item1);
                maxX = node.Children[quadrant].Items.Max(i => i.Item1);
            }
            else
            {
                minX = node.Children[quadrant].Items.Min(i => i.Item1);
                maxX = node.Children[quadrant].Items.Max(i => i.Item1);
            }

            if ((quadrant & 2) == 0)
            {
                minY = node.Children[quadrant].Items.Min(i => i.Item2);
                maxY = node.Children[quadrant].Items.Max(i => i.Item2);
            }
            else
            {
                minY = node.Children[quadrant].Items.Min(i => i.Item2);
                maxY = node.Children[quadrant].Items.Max(i => i.Item2);
            }

            if (x < minX)
            {
                if (y < minY)
                {
                    return CalculateDistance(x, y, minX, minY);
                }
                else if (y > maxY)
                {
                    return CalculateDistance(x, y, minX, maxY);
                }
                else
                {
                    return minX - x;
                }
            }
            else if (x > maxX)
            {
                if (y < minY)
                {
                    return CalculateDistance(x, y, maxX, minY);
                }
                else if (y > maxY)
                {
                    return CalculateDistance(x, y, maxX, maxY);
                }
                else
                {
                    return x - maxX;
                }
            }
            else
            {
                if (y < minY)
                {
                    return minY - y;
                }
                else if (y > maxY)
                {
                    return y - maxY;
                }
                else
                {
                    return 0;
                }
            }
        }

        return double.MaxValue;
    }

    private double CalculateDistance(double x1, double y1, double x2, double y2)
    {
        return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    private void Subdivide(Node node, int depth)
    {
        double minX = node.Items.Min(i => i.Item1);
        double minY = node.Items.Min(i => i.Item2);
        double maxX = node.Items.Max(i => i.Item1);
        double maxY = node.Items.Max(i => i.Item2);

        double midX = (minX + maxX) / 2;
        double midY = (minY + maxY) / 2;

        for (int i = 0; i < 4; i++)
        {
            node.Children[i] = new Node();

            foreach (var item in node.Items)
            {
                int quadrant = 0;
                if (item.Item1 >= midX) quadrant |= 1;
                if (item.Item2 >= midY) quadrant |= 2;

                if (quadrant == i)
                {
                    node.Children[i].Items.Add(item);
                }
            }

            if (node.Children[i].Items.Count > MaxItemsPerNode && depth < 10)
            {
                Subdivide(node.Children[i], depth + 1);
            }
        }

        node.Items.Clear();
    }


}

class Program
{
    public static void Main(string[] args)
    {
        Quadtree<string> quadtree = PopulateQuadtreeFromBinaryDataFile("C:\\Users\\Lumkile\\Desktop\\MixT\\VehiclePositions.dat");

        double[][] coordinates = new double[10][]
        {
            new double[] { 34.544909, -102.100843 },
            new double[] { 32.345544, -99.123124 },
            new double[] { 33.234235, -100.214124 },
            new double[] { 35.195739, -95.348899 },
            new double[] { 31.895839, -97.789573 },
            new double[] { 32.895839, -101.789573 },
            new double[] { 34.115839, -100.225732 },
            new double[] { 32.335839, -99.992232 },
            new double[] { 33.535339, -94.792232 },
            new double[] { 32.234235, -100.222222 }
        };

        for (int i = 0; i < coordinates.Length; i++)
        {
            double testX = coordinates[i][0];
            double testY = coordinates[i][1];

            List<string> nearestNeighbors = quadtree.FindNearest(testX, testY);

            Console.WriteLine($"Coordinate ({testX:F6}, {testY:F6}): Nearest Vehicle Registrations:");
            foreach (string neighbor in nearestNeighbors)
            {
                Console.WriteLine(neighbor);
            }
            Console.WriteLine();
        }
    }

    private static string ReadNullTerminatedString(BinaryReader reader)
    {
        List<byte> bytes = new List<byte>();
        byte b;

        while ((b = reader.ReadByte()) != 0)
        {
            bytes.Add(b);
        }

        return Encoding.ASCII.GetString(bytes.ToArray());
    }

    private static Quadtree<string> PopulateQuadtreeFromBinaryDataFile(string filePath)
    {
        Quadtree<string> quadtree = new Quadtree<string>(30.0, -110.0, 40.0, -90.0);

        using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
        using (BinaryReader reader = new BinaryReader(fileStream))
        {
            while (reader.BaseStream.Position + 29 <= reader.BaseStream.Length)
            {
                int vehicleId = reader.ReadInt32();
                string vehicleRegistration = ReadNullTerminatedString(reader);
                float latitude = reader.ReadSingle();
                float longitude = reader.ReadSingle();
                ulong recordedTimeUTC = reader.ReadUInt64();

                double latitudeDouble = (double)latitude;
                double longitudeDouble = (double)longitude;

                quadtree.Insert(latitudeDouble, longitudeDouble, vehicleRegistration);
            }
        }

        return quadtree;
    }
}
